def vowel_swapper(string):
    #setting up replace method

    string1 = string
    
    string1 = string1.replace("a", "4")
    string1 = string1.replace("A", "4")
    string1 = string1.replace("e", "3")
    string1 = string1.replace("E", "3")
    string1 = string1.replace("i", "!")
    string1 = string1.replace("I", "!")
    string1 = string1.replace("o", "ooo")
    string1 = string1.replace("O", "000")
    string1 = string1.replace("u", "|_|")
    string1 = string1.replace("U", "|_|")
    
    return string1
    

#prints outputs

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console