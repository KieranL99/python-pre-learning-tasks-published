def factors(number):
    # setting up function

    list = []
    for i in range(1, number + 1):
       if number % i  == 0 and i != 1 and number != i:
           list.append(i) # identifies prime numbers from input and appends list

    if len(list == 0):
        list.append(str(number) + " is a prime number") # converts output to a string value stating that number is a prime number

    return list

#prints output
print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
